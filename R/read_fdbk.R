#' Load the entire content of a fdbk file
#' 
#' @param filename NetCDF fdbk filename including path
#' 
#' @return a list of entries from the given fdbk file
#' @author Felix <felix.fundel@@dwd.de>
#' @examples
#' fdbk = read_fdbk("~/examplesRfdbk/icon/synop/verSYNOP.2014120112")
#' str(fdbk)
read_fdbk <- function(filename){
  f              = open.nc(filename)
  n_global_attr  = file.inq.nc(f)$ngatts
  g_names        = c(); for (i in 0:(n_global_attr-1)) g_names=c(g_names,att.inq.nc(f,"NC_GLOBAL",i)$name)
  GLOBALS        = list(); for (i in 0:(n_global_attr-1)) GLOBALS[[i+1]] = att.get.nc(f,"NC_GLOBAL",i)
  names(GLOBALS) = g_names
  
  DATA     = list()
  for (i in 0:(file.inq.nc(f)$nvars-1)){
    DATA[[i+1]]           = var.inq.nc(f,i)
    for (j in 0:(DATA[[i+1]]$natts-1)){
      attname = att.inq.nc(f, DATA[[i+1]]$name, j)$name
      eval(parse(text=paste("DATA[[i+1]]$`",attname,"`  = att.get.nc(f,DATA[[i+1]]$name,'",attname,"')",sep="")))
    }
    DATA[[i+1]]$values    = var.get.nc(f,DATA[[i+1]]$name,collapse=F)
  }
  
  short       = do.call("rbind", lapply(DATA, "[[", 2))
  names(DATA) = short
  
  DIMENSIONS = list(); for (i in 0:(file.inq.nc(f)$ndims-1)){ DIMENSIONS[[i+1]] = dim.inq.nc(f,i) }
  names(DIMENSIONS) = lapply(DIMENSIONS,"[[","name")
  
  close.nc(f)
  return(list(DIMENSIONS=DIMENSIONS,GLOBALS=GLOBALS,DATA=DATA))
}
