#' Convert u,v wind in wind direction in degrees
#'
#' @param u u wind vector
#' @param v v wind vector
#'
#' @return wind direction in degree (0 - <360), 360 is set to 0, if u&v=0 then return NA
#'
#' @author Felix <felix.fundel@@dwd.de>
#' @examples
#' u = c( 10,   0,   0, -10,  10,  10, -10, -10, 0)
#' v = c(  0,  10, -10,   0,  10, -10,  10, -10, 0)
#' windDir(u,v)
windDir <- function(u, v){
  radi                               = 1/0.0174532925199433
  direction                          = atan2(u,v)*radi + 180
  direction[round(direction,8)==360] = 0
  direction[u==0 & v==0]             = NA
  return(direction)
}
