# Rfdbk
Working with COSMO feedback files.<br/>

## Installation
Download and install this package

```
> git clone git@gitlab.com:rfxf/Rfdbk.git
> R CMD INSTALL Rfdbk
```

## Example Files
Download example feedback files (1.1Gb) from: <br/>
https://drive.google.com/file/d/0BwjoGSRjc_8NWVBMQXpNY0FpWmM/view?usp=sharing <br/>
and unpack to your $HOME directory.
